<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@include file="taglibs.jsp" %>

<div id="db_kj_tiaoqian">
	<div class="db_kj_2">
		<div class="db_kj_3">
			<div class="db_kj_4">
				<img width="100" height="21" src="<c:url value='/images/weibu_1.jpg' />" />
			</div>
			<ul>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">注    册</a>
					</span>
					開設一個賬戶即可享受华博線上娛樂所有的精彩遊戲更有機會獲取高達30%的開戶紅利和0.9%無上限返水。
				</li>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">加盟合作</a>
					</span>
					現在開始加入利博的合作夥伴項目，坐享高達45%的佣金。
				</li>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">协议条款</a>
					</span>
					作為一個信譽好及富有強烈社會責任感的博彩公司，利博致力將博彩遊戲帶來的負面影響降至最低。
				</li>
			</ul>
		</div>
		<div class="db_kj_3">
			<div class="db_kj_4">
				<img width="100" height="21" src="<c:url value='/images/weibu_2.jpg' />" />
			</div>
			<ul>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">体育游戏介绍</a>
					</span>
					我們提供投注的體育賽事如英超、西班牙聯賽、冠軍杯、法甲聯賽、德甲，意甲、以及女籃、排球、等。
				</li>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">真人娱乐介绍</a>
					</span>
					在我們的真人娛樂城，玩法眾多如真人百家樂，真人骰寶，真人龍虎,真人輪盤,真人21點,溫州牌九等等。
				</li>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">彩票游戏介绍</a>
					</span>
					彩票遊戲具有開獎快速、中獎高、趣味性強等特點,官方同步賽果,公平公正.目前歷史最悠久的彩票之一。
				</li>
			</ul>
		</div>
		<div class="db_kj_6">
			<div class="db_kj_4">
				<img width="105" height="18" src="<c:url value='/images/weibu_3.jpg' />" />
			</div>
			<ul>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">优惠活动</a>
					</span>
					註冊享有高達30%首存紅利，最高30萬RMB，週返水最高0.9%。
				</li>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">帮助中心</a>
					</span>
					您可以通過常見問題欄目得到更多關於開戶，存款，提款和遊戲投注相關的咨訊幫助。
				</li>
				<li class="db_kj_5">
					<span>
						<a target="_blank" href="#">关于我们</a>
					</span>
					如果您有關於遊戲或者是娛樂場方面的疑問，您隨時可通過在線客服、QQ客服或者電話客服獲取幫助。
				</li>
			</ul>
		</div>
	</div>
</div>