package _mh2.security.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
//import _mh2.constants.SessionParameter;
//import _mh2.model.Backenduser;
import _mh2.util.Utility;

/**
 * @author marcheruela
 */
public class RequiredRoleSecurityTag extends TagSupport {

	private static final long serialVersionUID = 6814941458808554986L;
	
	private String requiredRole = null;
			
	public void setRequiredRole(String requiredRole) {
		this.requiredRole = requiredRole;
	}
	
	public String getRequiredRole() {
		return requiredRole;
	}

	@Override
	public int doStartTag() throws JspException {
//		Backenduser backenduser = (Backenduser) pageContext.getSession().getAttribute(SessionParameter.SESSION_USR);
		
//		if(backenduser == null) {
//			return SKIP_BODY;
//		}
//		
//		if(Utility.isEmpty(requiredRole)) {
//			return SKIP_BODY;
//		}
//		
//		String role = backenduser.getRole();
//		
//		if(Utility.isEmpty(role)) {
//			return SKIP_BODY;
//		}
//		
//		if(requiredRole.indexOf(role) == -1) {
//			//-- this is cheating
//			return SKIP_BODY;
//		}
		
		return EVAL_BODY_INCLUDE;
	}
}
