package _mh2.security.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
//import _mh2.constants.SessionParameter;
//import _mh2.constants.SystemConstant;
//import _mh2.model.Backenduser;
//import _mh2.model.Backenduserrole;
import _mh2.util.Utility;

/**
 * @author marcheruela
 */
public class HasAnyRoleSecurityTag extends TagSupport {

	private static final long serialVersionUID = -7942413152756879386L;
	private boolean hasAnyRole = false;
	
	public void setHasAnyRole(boolean hasAnyRole) {
		this.hasAnyRole = hasAnyRole;
	}
	
	public boolean getHasAnyRole() {
		return this.hasAnyRole;
	}
	
	@Override
	public int doStartTag() throws JspException {
//		return evaluate();
		return 1;
	}

//	private int evaluate() {
//		return hasAnyRole ? checkRole() : EVAL_BODY_INCLUDE;
//	}

//	private int checkRole() {
//		Backenduser user = (Backenduser) pageContext.getSession().getAttribute(SessionParameter.SESSION_USR);
//		
//		if(user == null)
//			return SKIP_BODY;
//		
//		String userrole = user.getRole();
//		
//		if(Utility.isEmpty(userrole))
//			return SKIP_BODY;
//		
//		for(Backenduserrole r : SystemConstant.backenduserroles) {
//			if(userrole.equals(r.getCode())) {
//				return EVAL_BODY_INCLUDE;
//			}
//		}
//		
//		return SKIP_BODY;
//	}
}
