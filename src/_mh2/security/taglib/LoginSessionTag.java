package _mh2.security.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

//import _mh2.constants.SessionParameter;
//import _mh2.model.Backenduser;

/**
 * @author marcheruela
 */
public class LoginSessionTag extends TagSupport {

	private static final long serialVersionUID = 2580740888916288718L;
	private String field;

	public void setField(String field) {
		this.field = field;
	}
	
	public String getField() {
		return field;
	}
	
	@Override
	public int doStartTag() throws JspException {
//		Backenduser backenduser = (Backenduser) pageContext.getSession().getAttribute(SessionParameter.SESSION_USR);
		
//		try {
//			JspWriter out = pageContext.getOut();
//			if("name".equals(field)) {
//				out.print(backenduser.getName());
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		return super.doStartTag();
	}
}
