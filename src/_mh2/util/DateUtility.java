package _mh2.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author marcheruela
 */
public final class DateUtility {

	/**
	 * @return returns the current Timestamp value
	 */
	public static Timestamp getCurrentTimestamp() {
		Calendar calendar = Calendar.getInstance();
		Timestamp curr = new Timestamp(calendar.getTimeInMillis());
		return curr;
	}
	
	public static String getFormattedString(Timestamp date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
}