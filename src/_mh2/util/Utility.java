package _mh2.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import _mh2.exception.BaseException;

/**
 * @author marcheruela
 */
public final class Utility {

	/**
	 * Concatenates the given array of String with a delimiter
	 * 
	 * @param stringList
	 * @param delimiter
	 * @return
	 */
	public static String arrayToString(List<String> stringList, String delimiter) {
		if (isEmpty(stringList)) {
			return "";
		}

		StringBuilder sb = new StringBuilder();

		for (String s : stringList) {
			sb.append(s);
			sb.append(delimiter);
		}

		/*
		 * returns the concatenated string excluding the last delimiter
		 */
		return sb.substring(0, sb.length() - delimiter.length());
	}

	/**
	 * Concatenates the given array of String
	 * 
	 * @param stringList
	 * @return
	 */
	public static String arrayToString(List<String> stringList) {
		return arrayToString(stringList, "");
	}

	/**
	 * Returns the returned value of the annotated method
	 * 
	 * @param o
	 *            - the instance
	 * @param clazz
	 *            - the annotation
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object getAnnotatedValue(Object o, Class clazz)
			throws BaseException {
		Object annotatedValue = null;

		for (Method method : o.getClass().getMethods()) {
			if (method.isAnnotationPresent(clazz)) {
				try {
					annotatedValue = method.invoke(o);

					break;
				} catch (Exception e) {
					throw new BaseException(e.getMessage());
				}
			}
		}

		if (annotatedValue == null)
			throw new BaseException("Unable to find annotation "
					+ clazz.getSimpleName());
		else
			return annotatedValue;

	}

	/**
	 * Standard way to retrieve the IP of the request origin
	 */
	public static String getRealIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * Checks if object is NULL
	 */
	public static boolean isNull(Object obj) {
		return (obj == null);
	}

	/**
	 * Checks if object is NULL OR EMPTY (collection) OR BLANK (String)
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(Object o) {
		if (o instanceof String) {
			return StringUtils.isBlank((String) o);
		} else if (o instanceof Collection) {
			return ((Collection) o).isEmpty();
		} else {
			return isNull(o);
		}

	}

	/**
	 * Checks if object is not empty
	 */
	public static boolean isNotEmpty(Object o) {
		return !isEmpty(o);
	}

	/**
	 * Returns true if at least one of the object in the vararg is empty
	 */
	public static boolean hasEmpty(Object... obj) {

		for (Object o : obj) {
			if (isEmpty(o))
				return true;
		}

		return false;
	}

	/**
	 * Checks if the vargarg has no empty element, that is none is null, blank
	 * (String) or empty (collection)
	 */
	public static boolean hasNoEmpty(Object... obj) {
		return !hasEmpty(obj);
	}

	/**
	 * Checks if all elements in the vararg are all empty (collection) or blank
	 * (string) or null.
	 */
	public static boolean isAllEmpty(Object... obj) {
		for (Object o : obj) {
			if (!isEmpty(o))
				return false;
		}

		return true;
	}

	/**
	 * Checks if all elements in the vararg are NOT empty (collection) nor blank
	 * (string) nor null.
	 */
	public static boolean isAllNotEmpty(Object... obj) {
		return !hasEmpty(obj);
	}

	/**
	 * Validates if the vararg is not EMPTY, otherwise throw exception
	 */
	public static void validate(String errorMessage, Object... obj)
			throws BaseException {
		if (hasEmpty(obj)) {
			throw new BaseException(errorMessage);
		}
	}

	/**
	 * Returns the default value if the object is empty, otherwise returns the
	 * object
	 */
	public static <T> T ifNull(T o, T defaultVal) {
		return isEmpty(o) ? defaultVal : o;
	}

	/**
	 * Returns true if the two big decimals are equal, given the number of
	 * decimal places
	 */
	public static boolean areEqualBigDecimal(BigDecimal b1, BigDecimal b2,
			int decimalPlaces) {
		if (b1 == null || b2 == null)
			return false;

		StringBuffer sb = new StringBuffer("0.");

		for (int c = 0; c < decimalPlaces; c++) {
			sb.append("0");
		}

		DecimalFormat df = new DecimalFormat(sb.toString());
		df.setRoundingMode(RoundingMode.DOWN);

		return df.format(b1).equals(df.format(b2));
	}
}
