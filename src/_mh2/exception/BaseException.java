package _mh2.exception;

/**
 * Base custom exception for this application
 *  
 * @author marcheruela
 */
public class BaseException extends Exception {

	private static final long serialVersionUID = 1703896528927606708L;

	private String errMessage;
	
	//-- Force developer to describe the error
	public BaseException(String errMessage) {
		this.errMessage = errMessage;
	}
	
	public String getErrMessage() {
		return this.errMessage;
	}
	
	public String getMessage() {
		return this.errMessage;
	}
}